<?php

namespace Drupal\reference_number_trimmer\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_reference_autocomplete_noid' widget.
 *
 * Hides Ids from entity reference autocomplete widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_autocomplete_noid",
 *   label = @Translation("Autocomplete (hidden IDs)"),
 *   description = @Translation("An autocomplete text field that doesn't show entity IDs."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceAutocompleteWithoutIdWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element = parent::formElement($items, $delta, $element, $form,
      $form_state);
    $element['target_id']['#attributes']['data-hidden-id'][] = "on";
    $element['target_id']['#attached']['library'][] = "reference_number_trimmer/reference_number_trimmer";
    return $element;
  }

}

<?php

namespace Drupal\reference_number_trimmer\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteTagsWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implementation of the 'entity_reference_autocomplete_tags_noid' widget.
 *
 * Hides Ids from entity reference autocomplete widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_autocomplete_tags_noid",
 *   label = @Translation("Autocomplete (Tags style, hidden IDs)"),
 *   description = @Translation("An autocomplete text field with tagging support that doesn't show entity IDs."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class EntityReferenceAutocompleteTagsWithoutIdWidget extends EntityReferenceAutocompleteTagsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['target_id']['#attributes']['data-hidden-id'][] = "on";
    $element['target_id']['#attached']['library'][] = "reference_number_trimmer/reference_number_trimmer";
    return $element;
  }

}

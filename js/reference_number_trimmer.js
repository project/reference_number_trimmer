(function (Drupal) {

  const trimmer_regex = /(\s*\([0-9]*\))"?$/

  Drupal.behaviors.autocompleteReferenceEntityId = {
    detach: function (context) {},
    attach: function (context) {
      let forms = Array.from(context.querySelectorAll("form"))
      if (context instanceof HTMLFormElement) {
        forms.push(context)
      }
      forms.forEach(
        function (form) {
          form.onsubmit = function () {
            let elements = this.querySelectorAll("[data-hidden-id=\"on\"]")
            elements.forEach(
              function (element) {
                const values = Drupal.autocomplete.splitValues(element.value)
                const realValues = values.map(value => element._dataMap.get(value) ?? value)
                element.value = realValues.join(', ')
              }
            )
            return true
          }
        }
      )

      let elements = once("replaceReferenceIdOnInit", "[data-hidden-id=\"on\"]", context)
      elements.forEach(
        function (element) {
          const splitValues = Drupal.autocomplete.splitValues(element.value)
          const dataMap = new Map()
          splitValues.forEach(value => {
            value = value.trim()
            const entityIdMatch = value.match(trimmer_regex)
            const key = entityIdMatch === null ? value : value.replace(entityIdMatch[1], '')
            dataMap.set(key, value)
          })
          element._dataMap = dataMap
          element.value = Array.from(dataMap.keys()).join(', ')
        }
      )
    }
  }

  let autocomplete = Drupal.autocomplete.options

  autocomplete.select = function (event, ui) {
    const target = event.target

    const terms =  Drupal.autocomplete.splitValues(event.target.value)
    terms.pop()

    const originalValues = target._dataMap ?? {}
    const dataMap = new Map()

    terms.forEach(term => {
      dataMap.set(term, originalValues.get(term) ?? term)
    })

    const entityIdMatch = ui.item.value.match(trimmer_regex)
    const trimmedValue = ui.item.value.replace(entityIdMatch[1], "")
    terms.push(trimmedValue)
    dataMap.delete(trimmedValue)
    dataMap.set(trimmedValue, ui.item.value)

    target._dataMap = dataMap
    event.target.value = Array.from(target._dataMap.keys()).join(', ')
    return false
  }

})(Drupal)
